const express = require('express')
const app = express()
var port = process.env.PORT || 8080;
app.use (express.urlencoded({extended: false}))
const cors = require('cors');//New for microservice
app.use(cors());//New for microservice
app.listen(port) 
console.log("Microservice2 is running on port " + port)
app.get('/', (req, res) => {
    res.send("Microservice2 gateway by Team 9. Usage:host/quad?length=x&breadth=y");
})

app.get('/quad', function (req,res) {
    //fetching query parameters
    let length = req.query.length;
    let breadth = req.query.breadth;

    //calculating whether the dimensions represent a square
    let isSquare = length == breadth ? true : false;
    //calculating the area
    let area = length * breadth;

    if (isSquare) {
        res.send("The dimensions represent a square. The area is" + area);
    } else {
        res.send("The dimensions DO NOT represent a square. The area is" + area);
    }

})
